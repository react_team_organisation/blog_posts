import axios from 'axios';
export const FETCH_POSTS = 'FETCH_POSTS';
export const ADD_POST = 'ADD_POST';
export const FETCH_POST = 'FETCH_POST';
export const DELETE_POST = 'DELETE_POST';
const URL = 'http://reduxblog.herokuapp.com/api';
const API_KEY = '?key=mjaziri';
const urPost = `${URL}/posts${API_KEY}`;
export function fetchPosts(){
    const request = axios.get(urPost);
    return {
        type: FETCH_POSTS,
        payload: request,
    }
}

export function addPost(data, actionSucceded){
    const request =  axios.post(urPost, data, {
            headers: {
                'Content-Type': 'application/json',
            }
        }
    ).then(() => actionSucceded());
    return {
        type: ADD_POST,
        payload: request,
    }
}

export function fetchPost(id) {
    const fetchUrl = `${URL}/posts/${id}${API_KEY}`;
    const request = axios.get(fetchUrl);
    return {
        type: FETCH_POST,
        payload: request,
    }
}

export function deletePost(id, actionSucceded) {
    const deleteUrl = `${URL}/posts/${id}${API_KEY}`;
    const request = axios.delete(deleteUrl).then(() => {
        actionSucceded()
    });
    return {
        type: DELETE_POST,
        payload: request,
    }
}