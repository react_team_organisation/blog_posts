import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchPost, deletePost } from '../actions';
import { Link } from 'react-router-dom';

class PostShow extends Component {

    constructor(props){
        super(props);
        this.deletePost = this.deletePost.bind(this);
    }

    componentDidMount(){
        this.props.fetchPost(this.props.match.params.id);
    }

    deletePost(){
        this.props.deletePost(this.props.id, () => { this.props.history.push("/")});
    }

    render(){
        const { title, categories, content } = this.props;
        return(<div>
            <Link to="/">Back to List of posts</Link>
            <h3>{title}</h3>
            <h6>Categories: {categories}</h6>
            <p>{content}</p>
            <button onClick={()=> this.deletePost()} className="btn btn-danger pull-xs-right"> Delete Post </button>
        </div>);
    }
}

function mapStateToProps (state){
    return {
        id: state.posts.id,
        title: state.posts.title,
        categories: state.posts.categories,
        content: state.posts.content,
    }
}

function mapDispatchToProps( dispatch ){
    return bindActionCreators({ fetchPost, deletePost } , dispatch) ;
}

export default connect(mapStateToProps, mapDispatchToProps) (PostShow);