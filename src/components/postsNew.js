import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';
import { addPost } from "../actions";

class PostsNew extends Component {

    constructor(props){
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit(values){
        console.log(values);
        this.props.addPost(values,
         () => {this.props.history.push("/");}
    );
    }

    renderField(field){
        const { meta: { touched, error } } = field;
        const className = `form-group ${touched && error ? 'has-danger' : ''}`;
        return(<div className= {className}>
            <label>{field.label}</label>
            <input
                className="form-control"
                type="text"
                {...field.input}
            />
            <div className="text-help">
                {touched ? error : ''}
            </div>
        </div>);
    }

    render(){

        // handleSubmit is a prop of redux form
        const { handleSubmit } = this.props;
        return(
            <form onSubmit={handleSubmit(this.onSubmit)}>
                <Field
                    label="Title of the Post"
                    name="title"
                    component={this.renderField}
                />
                <Field
                    label="Categories"
                    name="categories"
                    component={this.renderField}
                />
                <Field
                    label="Content of the Post"
                    name="content"
                    component={this.renderField}
                />

                <button type="submit" className = "btn btn-primary">Submit</button>

                <Link className="btn btn-danger" to ="/">
                    Cancel
                </Link>
            </form>
        );
    }
}

function validate( values ){
    //console.log(values) -> { title: 'asdf", categories: 'asdf", content: 'asdf'}
    const errors = {};

    // Validate the inputs from 'values'
    if(!values.title){
        errors.title = 'Please enter a title for your post';
    }
    if(!values.categories){
        errors.categories = 'Please enter a categories for your post';
    }
    if(!values.content){
        errors.content = 'Please enter a content for your post';
    }

    // If errors is empty, the form is fine to submit
    // If errors has *any* properties, redxu form assumes form is invalid

    return errors;
}
function mapDispatchToProps(dispatch){
    return bindActionCreators({ addPost }, dispatch)
}

//PostsNew = connect(null, mapDispatchToProps)(PostsNew);

export default reduxForm({
    validate,
    form: 'PostsNewForm'
    }
)(connect(null, mapDispatchToProps)(PostsNew));