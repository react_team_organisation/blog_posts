import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import { fetchPosts } from '../actions';
import _ from 'lodash';

class PostsIndex extends Component {

    constructor(props){
        super(props);
       this.renderListPosts = this.renderListPosts.bind(this);
       this.redirectToAddPost = this.redirectToAddPost.bind(this);
    }

    componentDidMount(){
        this.props.fetchPosts();
    }

    renderListPosts(){
        return _.map(this.props.posts, post => {
            return (<li className="list-group-item" key = {post.id}>
                <Link to={`/posts/${post.id}`} >
                    {post.title}
                </Link>
            </li>);
        });
    }

    redirectToAddPost(event){
        event.preventDefault();

    }

    render(){
        return (
            <div>
                <div className="text-xs-right">
                    <Link className="btn btn-primary" to ="/posts/new">
                        Add Post
                    </Link>
                </div>
                <h3> Posts index </h3>
                <ul className="list-group">
                    {this.renderListPosts()}
                </ul>
            </div>
        );
    }
}

function mapStateToProps( state ){
   return { posts: state.posts };
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ fetchPosts }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(PostsIndex);