import {FETCH_POSTS, FETCH_POST, DELETE_POST} from '../actions';
import _ from 'lodash';

export default function(state = {}, action){
    switch(action.type){
        case FETCH_POSTS : {
            return _.mapKeys(action.payload.data, 'id');
        }
        case FETCH_POST : {
            return {
                ...state,
                id: action.payload.data.id,
                title : action.payload.data.title,
                categories : action.payload.data.categories,
                content : action.payload.data.content
            };
        }
    }
    return state;
}